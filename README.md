# KittySys

KittySys is a project to make a public domain, small kernel and operating system that is UNIX-compatible. **NOTE: This is an amateur project, and, unless I accidentally pull off voodoo magiks, I do not recommend daily use of this system.**

## Contributions

Contributions are very welcome! They must have the following requirements:
- Must be licensed CC0
- Must be small and efficient, at least minimal
- Must not rely on external libraries, ESPECIALLY if it's for the kernel

## License

The project's code and other assets are licensed under CC0.