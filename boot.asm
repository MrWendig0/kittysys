bits 32

section .multibott
	dd 0xE1E49A70			; ETERNAL!!!!!!!!!
	dd 0x0					; flags
	dd - (0xE1E49A70 + 0x0)	; checksum

section .text
global start
extern main

start:
	cli
	mov esp, stack_space
	call main
	hlt

section .bss
resb 8192
stack_space: