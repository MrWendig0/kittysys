// Main kernel code for the KittyKernel,
// part of the KittySys project

// Licensed under CC0

#define KSYS_ARCHITECTURE x86
// x86 is currently the ONLY architecture we're coding for

#ifndef KSYS_VIDEOMODE
#define KSYS_VIDEOMODE TEXT
// a text interface for things like VGA is used by default
#endif

char print(char *text);
char printColored(char *text, short color);
char clearScreen();

void main() {
	print("KittyKernel v0.0.1");
	return;
};