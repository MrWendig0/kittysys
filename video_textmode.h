#ifndef KSYS_TEXTMODE_HEADER
#define KSYS_TEXTMODE_HEADER

#ifdef KSYS_VGA
#define KSYS_SCREENSIZE 80 * 25

unsigned int *VGA_BUFFER;

char print(char *text) { // print white text
	short i = 0;
	while (text[i]) {
		VGA_BUFFER[i] = text[i];
		if (text[i] == '\n') i += 80; else i++;
	};
};

char printColored(char *text, short color) { // print VGA-colored text
	short i = 0;
	while (text[i]) {
		VGA_BUFFER[i] = text[i];
		if (text[i] == '\n') i += 80; else i++;
	};
};
#endif

#endif